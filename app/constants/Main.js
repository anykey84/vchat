import keyMirror from 'keymirror';

export default keyMirror({
    USER_REQUEST: null,
    USER_SUCCESS: null,
    USER_FAIL: null,
    AUTH_REQUEST: null,
    AUTH_SUCCESS: null,
    AUTH_FAIL: null,
    IDENT: null,
});