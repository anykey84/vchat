import keyMirror from 'keymirror';

export default keyMirror({
    FEED_REQUEST: null,
    FEED_SUCCESS: null,
    FEED_FAIL: null,
    VIDEO_REQUEST: null,
    VIDEO_SUCCESS: null,
    VIDEO_FAIL: null,
    CHANGE_STATUS_VIDEO: null,
    PAUSE_VIDEO: null,
    GET_PLAYER: null,
    PUSH_BOX: null,
    AUTH_MODAL: null,    
    COMMENT_REQUEST: null,
    COMMENT_SUCCESS: null,
    COMMENT_FAIL: null,    
    TAG_REQUEST: null,
    TAG_SUCCESS: null,
    TAG_FAIL: null,    
    LIKE_REQUEST: null,
    LIKE_SUCCESS: null,
    LIKE_FAIL: null,    
});