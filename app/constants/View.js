import keyMirror from 'keymirror';

export default keyMirror({
    LOAD_VIDEO: null,
    NEXT_VIDEO: null,
    PREV_VIDEO: null,
    LIKE_VIDEO: null,
    COMMENT_VIDEO: null,
    CREATE_TAG_VIDEO: null,
});