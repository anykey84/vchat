import React, { Component } from 'react';
import { connect } from 'react-redux';
import View from '../components/view/Main';


class ViewContainer extends Component {
  render() {
    return <View {...this.props} />;
  }
}

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    nextVideo: (count = 1,player)  => dispatch(nextVideo(count,player)),
		prevVideo: (count = -1,player) => dispatch(prevVideo(count,player)),
    getPlaylist: (id = 1)          => dispatch(getPlaylist(id)),
    likeVideo: (id = 1)            => dispatch(likeVideo(id)),
    createTag: (e)            => dispatch(createTag(e)),
  };
};
//,mapDispatchToProps
export default connect(mapStateToProps)(ViewContainer);