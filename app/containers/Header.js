import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Header from '../components/common/Header';
import  { ident,auth, logout } from '../actions/MainActions';
import  { showAuth } from '../actions/FeedActions';


class HeaderContainer extends Component {
  render() {
    return <Header {...this.props} />;
  }
}

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
     ident: bindActionCreators(ident,dispatch),
     auth: bindActionCreators(auth,dispatch),
     logout: bindActionCreators(logout,dispatch),
     showAuth: bindActionCreators(showAuth,dispatch),
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(HeaderContainer);