import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Feed from '../components/feed/Main';
import  { getPlayList,getPlayer,pushToBoxes,showAuth,addComment,addTag,like } from '../actions/FeedActions';


class FeedContainer extends Component {
  constructor(props){
    super(props)
    //console.log(props)
  }
  render() {    
    return <Feed {...this.props} />;
  }
}

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    getPlayList: bindActionCreators(getPlayList,dispatch),
    getPlayer: bindActionCreators(getPlayer,dispatch),
    pushToBoxes: bindActionCreators(pushToBoxes,dispatch),
    showAuth: bindActionCreators(showAuth,dispatch),
    like: bindActionCreators(like,dispatch),
    addTag: bindActionCreators(addTag,dispatch),
    addComment: bindActionCreators(addComment,dispatch),
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(FeedContainer);