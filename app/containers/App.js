import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Router, Route, browserHistory, hashHistory } from 'react-router';

import { connect } from 'react-redux';
import View from './View';


/*class App extends Component {
  render() {
    return ( 
    <div>
      <View/>
    </div>)
  }
}*/
ReactDOM.render(
      <Router history={browserHistory}>
          <Route path='/test' component={View} />
      </Router>
  ,
    document.getElementById('root')
);


export default App;