import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import mainReducer from '../reducers/main';

const createStoreWithMiddleware = applyMiddleware(thunkMiddleware)(createStore);

export default function mainStore(initialState) {
  const store = createStoreWithMiddleware(mainReducer, initialState);

  return store;
}