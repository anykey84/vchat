import React from 'react';
import ReactDOM from 'react-dom';

// import { Provider } from 'react-redux'
//
// import indexRoute from './routes/index'
//
// import mainStore from './store/main';
//
// const store = mainStore();

const HtmlCode = React.createClass({
    render: function () {
        return (
            <div id="container" className="container">
                <div className="row">
                    <div className="col-sm-6">
                        <a href="#">Васк</a>
                    </div>
                </div>
                <div className="row">
                    <h1 className="text-center">Direct and Manage Shoot</h1>
                </div>
                <div className="row">
                    <div className="video col-sm-8">
                        <div className="row">
                            <div className="col-sm-12">
                                <img className="img-responsive" src="img/rick.png" alt=""/>
                                <br/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="text-center">
                                <button className="btn btn-danger">Record</button>
                                <button className="microphone btn btn-primary">
                                    <img className="img-responsive" src="img/Microphone.png" alt=""/>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <p className="text-info">John: Blah Blah</p>
                        <p className="text-primary">Me: Agreed</p>
                        <p className="text-danger">More Blah-blah</p>
                        <p className="text-info">John: Blah Blah</p>
                        <p className="text-primary">Me: Agreed</p>
                        <p className="text-danger">More Blah-blah</p>
                        <p className="text-info">John: Blah Blah</p>
                        <p className="text-primary">Me: Agreed</p>
                        <p className="text-info">John: Blah Blah</p>
                        <form className="row" action="">
                            <div className="col-sm-9">
                                <textarea rows="2" className="form-control"></textarea>
                            </div>
                            <div className="col-sm-3">
                                <button><img src="img/paper-plane.svg" alt=""/></button>
                            </div>
                        </form>
                    </div>
                </div>
                <br/>
                <div className="row">
                    <div className="col-sm-12">
                        <ul className="nav nav-tabs">
                            <li className="active"><a data-toggle="tab" href="#panel1">Review/edit questions</a></li>
                            <li><a data-toggle="tab" href="#panel2">Download Clips</a></li>
                            <li><a data-toggle="tab" href="#panel3">Chat log</a></li>
                        </ul>

                        <div className="tab-content">
                            <div id="panel1" className="tab-pane fade in active">
                                <h3>Review/edit questions</h3>
                                <form action="">
                                    <div className="form-group">
                                        <div className="col-sm-1">Q1</div>
                                        <div className="col-sm-11">
                                            <input type="text" className="form-control" value="Edit question name" />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-sm-1">Q2</div>
                                        <div className="col-sm-11">
                                            <input type="text" className="form-control" value="Edit question name" />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-sm-1">Q3</div>
                                        <div className="col-sm-11">
                                            <input type="text" className="form-control" value="Edit question name" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="panel2" className="tab-pane fade">
                                <h3>Панель 2</h3>
                                <p>Содержимое 2 панели...</p>
                            </div>
                            <div id="panel3" className="tab-pane fade">
                                <h3>Панель 3</h3>
                                <p>Содержимое 3 панели...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
})


require("./../less/bootstrap.min.css");
require("./../less/main.less");

ReactDOM.render(
   //  <Provider store={store}>
   //      { indexRoute() }
   // </Provider>
    <HtmlCode />
  , document.getElementById('root')
);
