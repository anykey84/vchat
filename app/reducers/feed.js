// Reducer for /video/:id
import Constant from '../constants/Feed';

const initialState = {
    isLoading: true,
    player: [],
    video: null,
    boxes: [],
    authIsSHowing: false,
    addedTag: false,
    likedVideo: false,

}

export default function feed(state = initialState, action) {
    switch (action.type) {
        case Constant.FEED_REQUEST: {
            return {
                ...state,
                isLoading: true
            };
        }

        case Constant.FEED_SUCCESS: {
            return {
                ...state,
                error: null,
                list: action.list,
                isLoading: false,
                addedTag: false,
                likedVideo: false,
            };
        }

        case Constant.FEED_FAIL: {
            return {
                ...state,
                error: action.error,
                isLoading: false
            };
        }

        case Constant.VIDEO_REQUEST: {
            return {
                ...state,
                isLoading: true
            };
        }

        case Constant.VIDEO_SUCCESS: {
            return {
                ...state,
                error: null,
                video: action.video,
                isLoading: false
            };
        }

        case Constant.VIDEO_FAIL: {
            return {
                ...state,
                error: action.error,
                list: null,
                isLoading: false
            };
        }

        case Constant.COMMENT_REQUEST: {
            return {
                ...state,
            };
        }

        case Constant.COMMENT_SUCCESS: {
            return {
                ...state,
            };
        }

        case Constant.COMMENT_FAIL: {
            return {
                ...state,
                authIsSHowing: true,
            };
        }

        case Constant.TAG_REQUEST: {
            return {
                ...state,
            };
        }

        case Constant.TAG_SUCCESS: {
            return {
                ...state,
                addedTag: true,
            };
        }

        case Constant.TAG_FAIL: {
            return {
                ...state,
                authIsSHowing: true,
            };
        }

        case Constant.LIKE_REQUEST: {
            return {
                ...state,
            };
        }

        case Constant.LIKE_SUCCESS: {
            return {
                ...state,
                likedVideo: true,
            }
        }

        case Constant.LIKE_FAIL: {
            return {
                ...state,
                authIsSHowing: true,
            };
        }

        case Constant.GET_PLAYER: {
            return {
                ...state,
                player: [...state.player, action.player],
            };
        }

        case Constant.PUSH_BOX: {
            return {
                ...state,
                boxes: [...state.boxes, action.payload],
            };
        }

        case Constant.AUTH_MODAL: {
            return {
                ...state,
                authIsSHowing: action.payload,
            };
        }

        default: {
            return state;
        }
    }
};