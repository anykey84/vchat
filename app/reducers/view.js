// Reducer for /video/:id
import Constants from '../constants/View';

const initialVideo = {
  step: 0,
  player: null,
   playlist:null,
}

export default function info(state = initialVideo, action) {
  let newStep
  switch (action.type) {
    case Constants.LOAD_VIDEO:
      return {
        ...state,
         playlist: action.data
      }
    case Constants.LIKE_VIDEO:
      return {
        ...state,
        //  playlist: action.data
      }
    case Constants.NEXT_VIDEO:
      if (state.step >= state. playlist.length - 1) {
        newStep = 0
      }
      else {
        newStep = state.step + action.add
      }
      action.player.playVideoAt(newStep)
      return {
        ...state,
        step: newStep,
      };
    case Constants.PREV_VIDEO:
      if (state.step == 0) {
        newStep = state.video.length - 1
      }
      else {
        newStep = state.step + action.add
      }
      action.player.playVideoAt(newStep)
      return {
        ...state,
        step: newStep,
      };
    default:
      return state;
  };
};