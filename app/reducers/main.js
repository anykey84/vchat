import { combineReducers } from 'redux';
import view from '../reducers/view';
import feed from '../reducers/feed';
import Constants from '../constants/Main';

const initialState = {
    correctForm: true,
    authIsSHowing: false,
}

function header(state = initialState, action) {
    switch (action.type) {
        case Constants.USER_SUCCESS: {
            return {
                ...state,
                ident: action.payload
            };
        }
        case Constants.AUTH_SUCCESS: {
            return {
                ...state,
                auth: action.payload,
                correctForm: true
            };
        }
        case Constants.AUTH_FAIL: {
            return {
                ...state,
                correctForm: false
            };
        }

        default: {
            return state;
        }
    }
};

const mainReducer = combineReducers({
    view,
    feed,
    header,
});

export default mainReducer;