import Constants from '../constants/Main';
import FC from '../constants/Feed';
import api from '../api/video';
import cookie from 'react-cookie';

export const ident = (auth) => {
  return (dispatch) => {
    dispatch({
      type: Constants.USER_REQUEST
    });
    return api.ident(auth).then(ident => {
      dispatch({
        payload: ident.data,
        type: Constants.USER_SUCCESS
      })
    }
    );
  }
}

export const logout = () => {
  cookie.remove('jwt', { path: '/' })
  window.location.reload()
}

export const auth = (data) => {
  return (dispatch) => {
    dispatch({
      type: Constants.AUTH_REQUEST
    });
    return api.auth(data)
      .then(ident => {
        cookie.save('jwt', ident.data.token, {
          path: '/'
        });
        dispatch({
          payload: false,
          type: FC.AUTH_MODAL
        })
        dispatch({
          payload: ident.data,
          type: Constants.AUTH_SUCCESS
        })
      }
      )
      .catch(err =>
        dispatch({
          payload: err.data,
          type: Constants.AUTH_FAIL
        })
      )
  }

}


