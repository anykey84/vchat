import Constant from '../constants/Feed';
import api from '../api/video';

export function getPlayList() {
    return dispatch => {
        /*dispatch({
            type: Constant.FEED_REQUEST
        });*/

        return api.getPlayList().then(list =>
            dispatch({
                list: list.data,
                type: Constant.FEED_SUCCESS
            })
        );
    };
}

export function addComment(data) {
    return dispatch => {
        dispatch({
            type: Constant.COMMENT_REQUEST
        });

        return api.createComment(data)
            .then(list =>
                dispatch({
                    type: Constant.COMMENT_SUCCESS
                })
            ).catch(error =>
                dispatch({
                    type: Constant.COMMENT_FAIL
                })
            )
    };
}

export function addTag(data) {
    return dispatch => {
        dispatch({
            type: Constant.TAG_REQUEST
        });

        return api.createTag(data)
            .then(list =>
                dispatch({
                    type: Constant.TAG_SUCCESS
                })
            ).catch(error =>
                dispatch({
                    type: Constant.TAG_FAIL
                })
            )
    };
}

export function like(id) {
    return dispatch => {
        /*dispatch({
            type: Constant.LIKE_REQUEST
        });*/

        return api.like(id)
            .then(list =>
                dispatch({
                    type: Constant.LIKE_SUCCESS
                })
            ).catch(error =>
                dispatch({
                    type: Constant.LIKE_FAIL
                })
            )
    };
}

export function getVideo(id) {
    return dispatch => {
        dispatch({
            type: Constant.VIDEO_REQUEST
        });

        return api.getVideo(id).then(video =>
            dispatch({
                video: video.data,
                type: Constant.VIDEO_SUCCESS
            })
        );
    };
}

export function getPlayer(player) {
    return {
        player: player,
        type: Constant.GET_PLAYER
    }
}

export const pushToBoxes = (box) => {
    return {
        type: Constant.PUSH_BOX,
        payload: box,
    }
};

export const showAuth = (show) => {
    return {
        type: Constant.AUTH_MODAL,
        payload: show,
    }
};
