import Constants from '../constants/View';
import api from '../api/video';

const initialVideo = {
  step: 0,
  player: null,
  playlist:null,
}

export const nextVideo = (count = 1, player = null) => {
	return {
		type: Constants.NEXT_VIDEO,
		add: count,
    player: player,
	}
};

export const prevVideo = (count = -1, player = null) => {
	return {
		type: Constants.PREV_VIDEO,
		add: count,
    player: player,
	}
};

export const setVideo = (data) => {
	return {
		type: Constants.LOAD_VIDEO,
		data: data,
	}
};

export const getPlaylist = (id = 1) => {
  return (dispatch) => {
    api.getPlaylist(id)
       .then(
         response => dispatch(setVideo(response.data))
       )
     }
};

const like = (data) => {
	return {
		type: Constants.LIKE_VIDEO,
		data: data,
	}
};

export const likeVideo = (id = 1) => {
  return (dispatch) => {
    api.like(id)
       .then(
         response => dispatch(like(response.data))
       )
     }
};
