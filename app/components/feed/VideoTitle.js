import React, { Component, PropTypes } from 'react';

class VideoTitle extends Component {
    render() {
        const {avatar, username, title} = this.props
        return (
            <div className="video-title flex-between">
                <div className="userpic"><img src={avatar} /></div>
                <div className="video-info">
                    <div className="title">{title}</div>
                    <div className="name-and-views"><a href="#">{username}</a>
                        <p>921, 000 views</p>
                    </div>
                </div>
                <button className="btn video-answer-button"><strong>+Q</strong> Follow</button>
                <div className="dots">
                    <span className="dot"></span>
                    <span className="dot"></span>
                    <span className="dot"></span>
                </div>
            </div>
        );
    }
};

export default VideoTitle;