import React, { Component, PropTypes } from 'react';

class VideoTags extends Component {
  constructor(props) {
    super(props)
  }
  inputForTag(e) {
    e.preventDefault()
    this.formTags.classList.toggle('hidden')
  }
  addTag(e) {
    this.props.addTag({
      "name": this.tag.value,
      "video_id": this.props.id,
    })
    this.tag.value = ''
    this.formTags.classList.toggle('hidden')
    e.preventDefault()
  }
  render() {
    let { tags, likes, id, tagInputShow } = this.props;
    tags = tags == null ? [] : JSON.parse(tags);
    let tagsString = tags.join(' ')
    if (tagsString.length > 25) {
      tags = (tagsString.substring(0, 25) + '...').split(' ')
    }
    return (
      <div className="video-tags row">
        <div className="tags-and-more pull-left">
          <div className="tags">
            <div className="tags">
              {
                tags.map((tag, key) =>
                  <a key={key}>#{tag}</a>
                )
              }
              <a href="#" onClick={this.inputForTag.bind(this)}>
                <img src="img/plus-circle.png" />
              </a>
              <div className="hidden" ref={(div) => this.formTags = div} >
                <div className="form-inline">
                  <div className="form-group">
                    <input className="form-control" placeholder="Input tags"
                      ref={(input) => this.tag = input}
                      />
                  </div>
                  <button className="btn btn-default" onClick={this.addTag.bind(this)}>Ok</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="video-share pull-right">
          <a href={`http://80.87.195.10:3000/video/${id}`}>
            <img src="img/share.svg" alt="" />Share
          </a>
          <a href="#"><img src="img/like.svg" alt="" onClick={this.props.likeVideo} />{likes}</a>
          <button className="btn video-answer-button">Answer</button>

        </div>
      </div>
    );
  }
}



export default VideoTags;