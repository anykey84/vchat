import React, { Component, PropTypes } from 'react';

import Similar from '../common/Similar';
import Loader from '../common/Loader';
import VideoBox from './VideoBox';


class Feed extends Component {
  constructor(props) {
    super(props)
    props.getPlayList()
  }
  componentWillReceiveProps(nextProps) {
    if (
      (nextProps.feed.addedTag === true && this.props.feed.addedTag == false) ||
      (nextProps.feed.likedVideo === true && this.props.feed.likedVideo == false)
    ) {
      nextProps.getPlayList()
    }
  }
  render() {
    const { feed } = this.props
    const { isLoading, list } = feed

    if (!isLoading) {
      return (
        <div className="container maincontent uk-grid data-uk-grid-margin">
          <div className="leftcontent col-md-8">
            <div className="video-playlist">
              <div className="video-box" >
                {
                  list.map((list, index) =>
                    <VideoBox
                      {...this.props}
                      {...feed}
                      {...list}
                      key={list.id}
                      //getPlayer={getPlayer}
                      //pushToBoxes={pushToBoxes}
                      //addComment={addComment}
                      index={index}
                      />
                  )
                }
              </div>
            </div>
          </div>

          <Similar />
        </div>
      );
    }
    else {
      return <Loader />
    }
  }
}



export default Feed;