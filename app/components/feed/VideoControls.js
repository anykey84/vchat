import React, { Component, PropTypes } from 'react';

class VideoControls extends Component {
  
  componentWillReceiveProps (nextProps) {
    this.volumeInput.value = 100;
  }
  
  fullScreen() {
    const { id } = this.props
    let iframe = document.getElementById(`widget${id}`);
    if (iframe.requestFullscreen) {
      iframe.requestFullscreen();
    }
    else if (iframe.msRequestFullscreen) {
      iframe.msRequestFullscreen();
    }
    else if (iframe.mozRequestFullScreen) {
      iframe.mozRequestFullScreen();
    }
    else if (iframe.webkitRequestFullScreen) {
      iframe.webkitRequestFullScreen();
    }
  }
  nextVideo() {
    const {player, index} = this.props    
    player[index].nextVideo()
  }
  prevVideo() {
    const {player, index} = this.props    
    player[index].previousVideo()
  }
  playVideo() {
    const {player, index} = this.props          
    player[index].playVideo()
  }
  pauseVideo() {
    const {player, index} = this.props    
    player[index].pauseVideo()
  }
  render() {
    return (
      <div>
        <div className="side-left">
          <i className="fa fa-caret-left" aria-hidden="true" onClick={this.prevVideo.bind(this)}></i>
        </div>
        <div className="side-right">
          <i className="fa fa-caret-right" aria-hidden="true" onClick={this.nextVideo.bind(this)}></i>
        </div>
        <div className="like-button" >
          <i className="fa fa-heart-o" aria-hidden="true" onClick={this.props.likeVideo}/>
        </div>
        <div className="video-controls">
          <div className="play-button">
            <i className="fa fa-play" aria-hidden="true" onClick={this.playVideo.bind(this)} />
          </div>
          <div className="video-progress">
            <div className="video-seek" id="video-seek"></div>
          </div>
          <div className="fullscreen">
            <i className="fa fa-arrows-alt" aria-hidden="true" onClick={this.fullScreen.bind(this)} />
          </div>
          <div className="volume">
            <div className="volume-button">
            </div>
            <div className="volume-seek">
              <input min="0" max="100" step="1" type="range" ref={(input) => this.volumeInput = input} />
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default VideoControls;