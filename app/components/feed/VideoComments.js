import React, { Component, PropTypes } from 'react';


class VideoComments extends Component {
    sendComment(e){
        //console.log(this.commentInput.value);
        this.props.addComment({
            "video_id":this.props.id,
            "text":this.commentInput.value
        })
        e.preventDefault();
        this.commentInput.value = ""
    }
    render() {
        return (
            <div className="video-comments">
                <div className="video-add-comment">
                    <div>
                        <form>
                            <div className="row">
                                <div className="col-sm-9"><input type="text" placeholder="Add comment" className="form-control" 
                                    ref={(text) => this.commentInput = text} /></div>
                                <div className="col-sm-3 text-right">
                                    <div className="liter-counter">0/500</div>
                                    <button className="btn btn-default btn-block" onClick={this.sendComment.bind(this)}>
                                        <img src="img/paper-plane.svg" alt="" />
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}



export default VideoComments;