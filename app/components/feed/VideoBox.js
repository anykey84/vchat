import React, { Component, PropTypes } from 'react';
import YouTube from 'react-youtube'


import VideoTitle from './VideoTitle';
import VideoControls from './VideoControls';
import VideoTags from './VideoTags';
import VideoComments from './VideoComments';

import { opts } from '../../utils/YoutubeUtils.js'

let videos = [["0zKHIS3eFeM", "k0cWAi6GEc4", "OMczeLbWD3E", "OMa5sXMqK_E", "zTvZov4QwiQ"], ["OlqEibmw58M", "wE_U8jrZ8BM", "DNTN1DWNrTg", "B8b9nWMXTSI", "GWiu2tY4Oto"], ["dhxA3HxXKjA", "LFSvI-3PXIA", "hzRxRY1SQmo", "kfmoNQVTa0M", "YJNMUXo7x-c"], ["-dA-lEPqtZA", "kE6b9583tyY", "wgT4sjRiLAY", "8RNmiB2eOFo", "Dg-mf4l5S14"], ["F18nLcJvlwY", "CxU0bEOGBBU", "sy0N6i33n-w", "3VfWvherevc", "h9NUUsBe2lU"], ["dpuJPoXkFG4", "m1so1A2EsNw", "cpcXiTmMl_s", "xxxeZCKgC8s", "iW2Y55-OYRI"], ["RSGgl9jG9vo", "2r9gzTIA1Ko", "Z7NxYucSo5s", "DvbyjrFLD0o", "Gl6c4VUlK1c"], ["TKySzbA-lUs", "l95tDlB98Sk", "KP6209X7XBI", "4PHIDlzTstk", "XUQXk_vtYl4"], ["3u_baOgbrGo", "wJ5t6Fz6AaU", "8DtShWToofQ", "P4UVB1seKew", "AnU2qiKEl1Y"], ["leyf7lXaDdg", "yIu189kLMGI", "bSI0QV2FU_Q", "tNbSHl_XKKs", "guwBmh9JvyY"]];

class VideoBox extends Component {
  componentDidMount() {
    this.props.pushToBoxes(this.box)
  }

  componentWillReceiveProps(props) {
    const boxes = this.props.feed.boxes
    if (boxes.length > 0) {
      const {index, player} = props
      window.onscroll = function () {
        let scrolled = window.pageYOffset || document.documentElement.scrollTop;
        boxes.map((box, number) => {
          if (boxes[number].offsetTop - 150 < scrolled && scrolled < boxes[number].offsetTop + 150) {
            player[number].playVideo()
          }
        })
      }
    }

  }

  onReady(event) {
    const { v, getPlayer, index } = this.props
    event.target.cuePlaylist([v, ...videos[index]]).setLoop(true);
    getPlayer(event.target)
  }

  onStateChange(event) {
    if (event.data == 1) {
      const { player, index } = this.props
      player.forEach((player, number) => {
        if (number !== index) {
          player.pauseVideo()
        }
      })
    }
  }

  likeVideo(){
    this.props.like(this.props.id)
  }

  render() {
    const { player, username, avatar, title, tags, likes, id, addComment,addTag,tagInputShow } = this.props

    return (
      <div ref={(elem) => this.box = elem} >
        <div className="video-playlist">
          <div className="video-box">
            <div className="video">
              <VideoTitle
                username={username}
                avatar={avatar}
                title={title}
                />
              <YouTube
                id={"widget" + id}
                className="player"
                opts={opts}
                onReady={this.onReady.bind(this)}
                onStateChange={this.onStateChange.bind(this)}
                />
              <VideoControls
                player={player}
                id={id}
                index={this.props.index}
                likeVideo={this.likeVideo.bind(this)}
                />
            </div>
            <VideoTags
              tags={tags}
              likes={likes}
              tagInputShow={tagInputShow}
              addTag={addTag}
              id={id}
              likeVideo={this.likeVideo.bind(this)}
              />
            <VideoComments
              addComment={addComment}
              id={id}
              />

          </div>
        </div>
      </div>
    );
  }
}

export default VideoBox;