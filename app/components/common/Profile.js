import React, { Component } from 'react';
import cookie from 'react-cookie';

class Profile extends Component {
  logout(e){
    cookie.remove('jwt', { path: '/' })
    e.preventDefault()
    window.location.reload()
}
    render() {
        const {avatar} = this.props.ident;
        return (
          <div className="col-md-4">
            <div className="profile">
              <a href="#">
                <i className="fa fa-crosshairs" aria-hidden="true" />
              </a>
              <a href="#">
                <i className="fa fa-bell-o" aria-hidden="true" />
              </a>
              <a href="#" onClick = {this.logout}>
                <img src={avatar} alt="" />
              </a>
            </div>
          </div>
        );
    }
}

export default Profile;
