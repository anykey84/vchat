import React, { Component } from 'react';
import Auth from './Auth'
import Profile from './Profile'

class Header extends Component {
  constructor(props) {
    super(props)
    props.ident()
    //console.log(props);
  }
  authModal(e) {
    this.props.showAuth(true)
    e.preventDefault()
  }

  componentWillReceiveProps(nextProps) {
    //if(nextProps.feed.authIsSHowing&&)
    if(nextProps.feed.authIsSHowing === false && nextProps.feed.authIsSHowing!==this.props.feed.authIsSHowing){
      this.props.ident(true);      
    }
  }

  render() {
    const { auth, header } = this.props
    const {ident} = header
    let toolbar;
    if (ident) {
      toolbar = <Profile
        ident={ident}
        />
    }
    else {
      toolbar = <div className="login-button">
        <a href="/#" onClick={this.authModal.bind(this)}>Login</a>
      </div>
    }
    return (
      <div className="container">
        {this.props.feed.authIsSHowing ? <Auth
          auth={auth}
          correctForm={header.correctForm}
          /> : ''}
        <header className="flex">
          <div className="logo-container">
            <div className="logo">
              <a href="#"><img src="img/logo.png" alt="logo" /></a>
            </div>
          </div>
          <form className="search-or-ask" name="search-or-ask">
            <input type="text" name="ask" placeholder="search or ask" />
            <input type="submit" className="btn" value="Ask a Question" />
          </form>
          <div className="profile-container">
            {toolbar}
          </div>
        </header>
      </div>
    );
  }

};

export default Header;
