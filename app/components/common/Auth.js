import React, { Component } from 'react';

class Auth extends Component {
  sendLogin(e) {
    this.props.auth({
      "username": this.username.value,
      "password": this.password.value
    })
    e.preventDefault();
  }
  render() {
    const { correctForm } = this.props;
    return (
      <div id="login-form" className="modal fade in" role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h3>Eskimo</h3>
              <h4>ask me more</h4>
              <div className="modal-body">
                <div className="row">
                  <div className="col-xs-6">
                    <div className="soc-auth">
                      <a href="#" className="google-auth">
                        <i className="fa fa-google" aria-hidden="true"></i>
                        <span>Continue with Google</span>
                      </a>
                      <a href="#" className="facebook-auth">
                        <i className="fa fa-facebook-official" aria-hidden="true"></i>
                        <span>Continue with Facebook</span>
                      </a>
                      <a href="#" className="twitter-auth">
                        <i className="fa fa-twitter" aria-hidden="true"></i>
                        <span>Continue with Twitter</span>
                      </a>
                    </div>
                    <p className="email-auth">
                      <a href="#">Continue With Email.</a>
                      By signing up you indicate that you have read and agree to the Terms of Service and Privacy Policy.
                    </p>
                  </div>
                  <div className="col-xs-6">
                    <h5>Sign-IN</h5>
                    <div className="form-group">
                      <input className="form-control" type="text" name="login" placeholder="Login" value="demo@eskimo.ru"
                        ref={(input) => this.username = input} />
                      <label htmlFor="login">Login</label>
                    </div>
                    <div className="form-group">
                      <input className="form-control" type="password" name="password" placeholder="Password" value="123"
                        ref={(input) => this.password = input} />
                      <label htmlFor="password">Password</label>
                    </div>
                    {correctForm?'':<span className="text-danger">Incorrect login or password</span>}
                    <div className="row login-forgot">
                      <div className="col-xs-6 forgot-password">
                        <a href="#">Forgot Password?</a>
                      </div>
                      <div className="col-xs-6 text-right">
                        <input type="submit" value="Login" className="btn" onClick={this.sendLogin.bind(this)} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Auth;
