import React, { Component } from 'react';

const questions = [
    { "title": "Home Audio Recording For Everyone ?", "views": "123, 825", "thumb": "/img/thumb.jpg", "username": "Jackson Garner", "userimage": "/img/person.png" },
    { "title": "Home Audio Recording For Everyone ?", "views": "123, 825", "thumb": "/img/thumb.jpg", "username": "Jackson Garner", "userimage": "/img/person.png" },
    { "title": "Home Audio Recording For Everyone ?", "views": "123, 825", "thumb": "/img/thumb.jpg", "username": "Jackson Garner", "userimage": "/img/person.png" },
    { "title": "Home Audio Recording For Everyone ?", "views": "123, 825", "thumb": "/img/thumb.jpg", "username": "Jackson Garner", "userimage": "/img/person.png" },
]

class Similar extends Component {
    render() {
        return (
            <div className="rightcontent col-sm-12 col-md-3 col-lg-4">
                <div className="simillar-questions">
                    <h2>Simillar questions</h2>
                    {
                        questions.map((question, key) =>
                            <div className="simillar-question" key={key}>
                                <a href="#">
                                    <div className="thumb"><img src={question.thumb} alt="" /></div>
                                </a>
                                <div className="question-info">
                                    <h3>{question.title}</h3>
                                    <div className="views">{question.views}views</div>
                                    <a href="#" className="author"><img src={question.userimage} alt="" />
                                        Jackson Garner
                                </a>
                                </div>
                            </div>
                        )
                    }
                    <a href="#" className="more-questions">More questions</a>
                </div>
            </div>
        );
    }
};

export default Similar;
