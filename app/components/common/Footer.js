import React, { Component } from 'react';

class Footer extends Component {
    render() {
        const date = new Date()
        return (
            <footer className="container">
                <div className="row">
                    <div className="logo col-md-2">
                        <a href="#"><img src="img/logo.png" alt="logo" /></a>
                    </div>
                    <ul className="bottom-menu col-md-6">
                        <li><a href="/contact">Contact</a></li>
                        <li><a href="/about">About US</a></li>
                        <li><a href="/press">Press</a></li>
                        <li><a href="/contact">Blog</a></li>
                    </ul>
                    <div className="copyright col-md-4 pull-right">Copyright © {date.getFullYear()}, Escimo. All rights reserved.</div>
                </div>
            </footer>
        );
    }

}

export default Footer;
