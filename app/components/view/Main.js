import React, { Component, PropTypes } from 'react';
import YouTube from 'react-youtube'

import { getPlaylist } from '../../actions/ViewActions';
import { opts } from '../../utils/YoutubeUtils.js'

class View extends Component {
  constructor(props) {
    super(props)
    const { dispatch, view } = this.props;
    if (view.playlist === null) {
      dispatch(getPlaylist(props.params.id));
    }

    this.onReady = this.onReady.bind(this);

  }

  render() {
    const { dispatch, view } = this.props;
    
    return (
      <div>
        <div className="video-playlist">
          <div className="video-box">
            <div className="video">
              <YouTube
                id="player"
                className="youtube"
                opts={opts}
                onReady={this.onReady}
                />
              <div className="side-left">
                <i className="fa fa-caret-left" aria-hidden="true" ></i>
              </div>
              <div className="side-right">
                <i className="fa fa-caret-right" aria-hidden="true" ></i>
              </div>
              <div className="like-button" >
                <i className="fa fa-heart" aria-hidden="true" />
              </div>
              <div className="video-controls">
                <div className="play-button">

                  <i className="fa fa-pause hidden" aria-hidden="true" />
                </div>
                <div className="video-progress">
                  <div className="video-seek" id="video-seek"></div>
                </div>
                <div className="fullscreen">
                  <i className="fa fa-arrows-alt" aria-hidden="true" />
                </div>
                <div className="volume">
                  <div className="volume-button">
                  </div>
                  <div className="volume-seek">
                    <input id="slider1" min="0" max="100" step="1" type="range" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}



export default View;