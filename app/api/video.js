import axios from 'axios';
import cookie from 'react-cookie';

import { apiPrefix } from '../config/main.json';

let instance

if (cookie.load('jwt')) {
    instance = axios.create({
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${cookie.load('jwt')}`
        },
    });
}
else {
    instance = axios.create({
        headers: { 'Content-Type': 'application/json' },
    });
}

export default {
    auth(data) {
        return instance.post(`${apiPrefix}/auth`, data);
    },

    ident(auth = false) {
        if (auth == true) {

            instance = axios.create({
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${cookie.load('jwt')}`
                },
            });
        }
        return instance.get(`${apiPrefix}/ident`);
    },

    getQuestion(id) {
        return instance.get(`${apiPrefix}/question/${id}`);
    },

    getPlayList() {
        return instance.get(`${apiPrefix}/feed`);
    },

    getVideo(id) {
        return instance.get(`${apiPrefix}/video/${id}`);
    },

    getTags(id) {
        return axios.get(`${apiPrefix}/tags/${id}`);
    },

    createTag(data) {
        return instance.post(`${apiPrefix}/tag`, data);
    },

    createComment(data) {
        return instance.post(`${apiPrefix}/comment`, data);
    },

    like(id) {
        return instance.post(`${apiPrefix}/like`, {"video_id":id,"question":id});
    }

}
