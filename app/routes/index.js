import React from 'react'
import { Router, Route, browserHistory } from 'react-router'

import View from '../containers/View';
import Feed from '../containers/Feed';
import Header from '../containers/Header';
import Footer from '../components/common/Footer';

export default function () {
  return (
    <div>
      <Header />
      <hr />
      <Router history={browserHistory}>
        <Route path="/" component={Feed} />
        <Route path="video/:id" component={View} />
      </Router>
      <hr />
      <Footer />
    </div>
  )
}
